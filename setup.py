#! /usr/bin/env python
# -*- coding: utf-8 -*-
'''setup for shove for Redis'''

from setuptools import setup, find_packages


def getversion(fname):
    '''Get the __version__ without importing.'''
    with open(fname) as f:
        for line in f:
            if line.startswith('__version__'):
                return '%s.%s.%s' % eval(line[13:].rstrip())

setup(
    name='shove_redis',
    version=getversion('shove_redis/__init__.py'),
    description='Dictionaryish style object storage and caching with Redis',
    long_description=open('README.rst').read(),
    author='L. C. Rees',
    author_email='lcrees@gmail.com',
    url='https://bitbucket.org/lcrees/shove-redis',
    license='BSD',
    packages=find_packages(),
    test_suite='shove_redis.test',
    install_requires=['shove>=0.6.4', 'redis'],
    zip_safe=False,
    keywords='object storage persistence database redis cache',
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Database :: Front-Ends',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: Implementation :: PyPy',
        'Programming Language :: Python :: Implementation :: CPython',
        'Framework :: Setuptools Plugin',
    ],
    entry_points='''
    [shove.stores]
    redis=shove_redis.store:RedisStore
    [shove.caches]
    redis=shove_redis.cache:RedisCache
    ''',
)
