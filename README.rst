Dictionaryish style object storage and caching that supports dictionary-style access and object serialization and
compression using Redis.