# -*- coding: utf-8 -*-
'''Tests for shove-redis'''

from stuf.six import unittest

from shove.test.mixins import Spawn
from shove.test.test_store import Store


class TestRedisStore(Store, Spawn, unittest.TestCase):

    initstring = 'redis://localhost:6379/0'
    cmd = ['redis-server']

    def tearDown(self):
        if self.store._store is not None:
            self.store.clear()
            self.store.close()