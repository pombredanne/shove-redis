# -*- coding: utf-8 -*-
'''Tests for shove-redis'''

from stuf.six import unittest

from shove.test.mixins import Spawn
from shove.test.test_cache import Cache


class TestRedisCache(Cache, Spawn, unittest.TestCase):

    initstring = 'redis://localhost:6379/0'
    cmd = ['redis-server']